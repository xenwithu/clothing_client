import { Outlet } from "react-router-dom";
import AdminHeader from "../../components/AdminHeader/AdminHeader";
import AdminSidebar from "../../components/AdminSidebar/AdminSidebar";
import style from "./style.module.css"
const AdminPage = () => {
  return <>
    
    <div className={`row`}>
      <div className={`col-2 px-0`}>
        <AdminSidebar/>
      </div>
      <div className={`col-10 px-0`}>
        <div>
          <AdminHeader/>
        </div>
        <div className={`${style.content_inner}`}>
          <Outlet/>
        </div>
      </div>
    </div>
  </>;
};

export default AdminPage;
