import { RouterProvider, createBrowserRouter } from "react-router-dom";
import { Outlet } from "react-router-dom";
import { useAuth } from "../provider/authProvider";
import { ProtectedRoute } from "./ProtectedRoute";
import { ProtectedRouteAdmin } from "./ProtectedRoute_Admin";
import Login from "../pages/Login";
import Logout from "../pages/Logout";
import AdminPage from "../pages/Admin/AdminPage";
import Tab1 from "../pages/Admin/Tab/Tab1";
import Tab2 from "../pages/Admin/Tab/Tab2";
import Categories from "../pages/Admin/Tab/Categories/Categories";

const Routes = () => {
  const { token } = useAuth();

  // Define public routes accessible to all users
  const routesForPublic = [
    {
      path: "/service",
      element: <div>Service Page</div>,
    },
    {
      path: "/about-us",
      element: <div>About Us</div>,
    },
  ];

  // Define routes accessible only to authenticated users
  const routesForAuthenticatedOnly = [
    {
      path: "/",
      element: <ProtectedRoute />, // Wrap the component in ProtectedRoute
      children: [
        {
          path: "",
          element: <div>User Home Page</div>,
        },
        {
          path: "/profile",
          element: <div>User Profile</div>,
        },
        {
          path: "/logout",
          element: <Logout/>,
        },
      ],
    },
  ];

  const routesForAdmin=[
    {
      path: "admin",
      element: <ProtectedRouteAdmin/>, // Wrap the component in ProtectedRoute
      children: [
        {
          path: "",
          element: <AdminPage/>,
          children: [
            {
              path: "categories",
              element: <Categories/>,
            },
            {
              path: "tab2",
              element: <Tab2/>,
            },
            {
              path: "profile",
              element: <Outlet/>,
              children: [
                {
                  path: "profile",
                  element: <div>admin admin Profile</div>,
                },
              ],
            },
          ],
          
        },
        {
          path: "logout",
          element: <Logout/>,
        },
      ],
    },
  ]

  // Define routes accessible only to non-authenticated users
  const routesForNotAuthenticatedOnly = [
    {
      path: "/",
      element: <div>Home Page</div>,
    },
    {
      path: "/login",
      element: <Login/>,
    },
  ];

  // Combine and conditionally include routes based on authentication status
  const router = createBrowserRouter([
    ...routesForPublic,
    ...(!token ? routesForNotAuthenticatedOnly : []),
    ...routesForAdmin,
    ...routesForAuthenticatedOnly,
  ]);

  // Provide the router configuration using RouterProvider
  return <RouterProvider router={router} />;
};

export default Routes;
