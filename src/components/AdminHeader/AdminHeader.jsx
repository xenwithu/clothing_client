import style from "./style.module.css"

const AdminHeader = () => {
  return (
      <>
          <div className={`d-flex justify-content-between align-items-center ${style.header_wrapper}`}>
              <div></div>
              <div>
                  <div className={`d-flex align-items-center`}>
                    <div className={`p-3`}>
                      <div className={`${style.account_name}`}>
                        <span>Lê Văn Đat</span>
                      </div>
                      <div className={`${style.account_role}`}>
                        <span>Manager</span>
                      </div>
                    </div>
                    <div className={`${style.avatar}`}>
                        <img src={`https://scontent-lga3-1.xx.fbcdn.net/v/t39.30808-6/269917174_410446840775926_1373436513321859246_n.jpg?_nc_cat=106&ccb=1-7&_nc_sid=efb6e6&_nc_eui2=AeHIvvs33z_ZDbeS6VVuL1IxOC7wS_B1uhI4LvBL8HW6EjlKZLxjquUMwr_PvN9fzf6cmb9D3RSkBQQq6IH9AqrR&_nc_ohc=aWz872TiIbUAX_NBEPn&_nc_ht=scontent-lga3-1.xx&oh=00_AfC6amO76NyYz8cB5f7EEKirNHgg7Mei5-YvZS-xcN0_Tw&oe=65AACC12`} alt="Logo" />
                    </div>
                  </div>
              </div>
          </div>
      </>
  )
  
};

export default AdminHeader;
