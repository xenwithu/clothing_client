import { Link } from "react-router-dom";
import style from "./style.module.css"
import { BsUmbrella } from "react-icons/bs";
import TabItem from "../TabItem/TabItem";

const AdminSidebar = () => {
    return (
      <>
        <div className={`${style.sidebar_wrapper} ${style.sidebar}`}>
            <div >
               <div className={ `${style.brand_wrapper} ${style.brand}`}>
                     <h2>TheRow</h2>
               </div>
            </div>
            <TabItem to={`/admin/categories`} icon={<BsUmbrella/>} title={`Categories`} active={false}/>
            <TabItem to={`/admin/product`} icon={<BsUmbrella/>} title={`Product`} active={true}/>
            <TabItem to={`/admin/categories`} icon={<BsUmbrella/>} title={`Categories`} active={false}/>
            <TabItem to={`/admin/categories`} icon={<BsUmbrella/>} title={`Categories`} active={false}/>
        </div>
      </>
    );
};
export default AdminSidebar;
  