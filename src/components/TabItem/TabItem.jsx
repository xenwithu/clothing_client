import { Link } from "react-router-dom";
import style from "./style.module.css"

const TabItem = ({to,icon,title,active}) => {
  return (
    <div className={`${style.item} ${active?style.active:""} ${style.item_wrapper} d-flex py-4 my-2`}>
        <Link to={`${to}`}>
            <div className={`d-flex`}>
                <div className={`px-2`}>
                    {icon}
                </div>
                <div>
                    <span>{title}</span>
                </div>
            </div>
        </Link>
    </div>
  )
  
};

export default TabItem;
